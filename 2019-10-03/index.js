let direction = "right"; // Dirección a la que se mueven los aliens
const missiles = [];
const villainMissiles = [];
let lives = 3;
let score = 0;
let num_villians = 40;
let killed_enemies=0;

window.onload = () => {
  
  for (i = 0; i < num_villians; i++) generateVillain();

  const villains = document.querySelector(".villains_div");

  setInterval(() => moveVillains(villains), 100);
  setInterval(() => {
    moveShoot();
    moveVillainShoot();
  }, 100);
  setInterval(villainsShoot, 2000);
  setInterval(() => moveVillainsDown(villains), 5000);
};

/**
 * Función que se ejecuta cada 100ms para que
 * mueva a los aliens y si han llegado al final que
 * cambien de dirección.
 */
moveVillains = villains => {
  const coordenadas = villains.getBoundingClientRect();
  const pixelsToMoveVillainsDiv = 20;

  if (
    coordenadas.right + pixelsToMoveVillainsDiv <
      document.documentElement.clientWidth &&
    direction == "right"
  ) {
    moveRight(
      villains,
      document.documentElement.clientWidth,
      pixelsToMoveVillainsDiv
    );
  } else {
    direction = "left";
  }

  if (direction == "left" && coordenadas.left > pixelsToMoveVillainsDiv) {
    moveLeft(villains, pixelsToMoveVillainsDiv);
  } else {
    direction = "right";
  }
};

moveVillainsDown = villains => {
  const coordenadas = villains.getBoundingClientRect();
  const villain = document.querySelector(".villains_div>div:first-child");
  const heightToMove = villain.offsetHeight;

  const maxBottom = document.querySelector(".game").getBoundingClientRect()
    .bottom;

  if (coordenadas.bottom + heightToMove * 2 <= maxBottom) {
    villains.style.top = `${coordenadas.top + heightToMove}px`;
  }
};

/**
 * Genera un div con una imagen de un alien
 * Y la añade a un div contenedor
 */
generateVillain = () => {
  const board = document.querySelector(".villains_div");

  const villain = document.createElement("div");
  villain.classList="img_villano";
  const imgVillain = document.createElement("img");

  imgVillain.setAttribute(
    "src",
    "https://regmedia.co.uk/2013/03/04/invader.jpg?x=442&y=293&crop=1"
  );
  imgVillain.className = "theVillian";
  imgVillain.setAttribute("height", "100%");
  imgVillain.setAttribute("width", "100%");

  villain.appendChild(imgVillain);
  villain.setAttribute("style", "height:25%;width:10%");

  board.appendChild(villain);
};

////Límites y detección de choques
//Función que cambia de sentido al detectar un límite
const changeDirection = AxeX => {
  sentido = true;
  if (AxeX + 100 >= 500) {
    sentido = false;
  } else if (AxeX - 100 <= 0) {
    sentido = true;
  }

  return sentido;
};

//Reconoce coordenada actual del elemento y busca si coincide con algún elemento malo, malvado o malévolo
const makeImpact = (element, colliderClassname) => {
  const { x, y } = element.getBoundingClientRect();
  const elems = document.elementsFromPoint(x, y);

  let impacted = null;
  elems.forEach(el => {
    if (el.className === colliderClassname) {
      impacted = el;
      element.parentElement.removeChild(element);
      killed_enemies = killed_enemies+1;
      console.log(killed_enemies);
      if (killed_enemies===num_villians){
        setTimeout(function(){
          alert("Ganaste el juego !");
          window.location.href = window.location.href;
        },1000);
      }
      if  (colliderClassname=="heroe_div"){
       // console.log("Le dieron al heroe");
        lives = lives-1;
        document.querySelector(".vidas").innerHTML=lives

        console.log(lives);
        if (lives==0){
          alert("Se acabo el juego!");
          window.location.href = window.location.href;
        }
      }
      else{
        score = score+10;
        //console.log(score);
        document.querySelector(".score").innerHTML=score;
      }
      return;
    }
  });
  return impacted;
};

//Evento para reconocer que tecla se esta tecleando
//Y llamar a las funciones moveLeft y moveRight respectivamente
keyDown = event => {
  let izquierda = "%";
  let derecha = "'";
  let disparar = "X";
  let key = event.keyCode || event.which;
  let keychar = String.fromCharCode(key);
  let container_width = document.querySelector(".gameContainer").offsetWidth;
  let heroe = document.querySelector(".heroe_div");
  if (keychar == izquierda) {
    moveLeft(heroe);
  }
  if (keychar == derecha) {
    moveRight(heroe, container_width);
  }
  if (keychar == disparar) {
    //console.log("shooot");
    //Hago un push al array de misiles con las coordenadas del heroe
    // el origen de los misiles estara a la para del heroe

    genShoot(heroe, "up");
  }
};

//función para movimiento a la izquierda
moveLeft = (heroe, pixelsToMove = 15) => {
  var coordenadas = heroe.getBoundingClientRect();
  if (coordenadas.left > 0) {
    heroe.style.left = `${coordenadas.left - pixelsToMove}px`;
  }
};

//función para movimiento a la derecha
moveRight = (heroe, width, pixelsToMove = 15) => {
  var coordenadas = heroe.getBoundingClientRect();
  if (coordenadas.right + pixelsToMove < width) {
    heroe.style.left = `${coordenadas.left + pixelsToMove}px`;
  }
};

//función para generar disparos
genShoot = (fromElement, direction = "up") => {
  const gameCont = document.querySelector(".gameContainer");
  const shoot = document.createElement("div");
  const coordenadasElement = fromElement.getBoundingClientRect();

  shoot.style.zIndex = 1;
  shoot.setAttribute(
    "style",
    `height:12px;${
      direction === "up" ? "background: lightskyblue" : "background: red"
    }`
  );

  shoot.style.left = `${coordenadasElement.left +
    fromElement.offsetWidth / 2}px`;

  if (direction === "up") {
    shoot.classList = "missile";
    shoot.style.top = `${coordenadasElement.top}px`;
  } else {
    shoot.classList = "missileV";
    shoot.style.top = `${coordenadasElement.bottom}px`;
  }

  gameCont.appendChild(shoot);

  if (direction === "up") {
    missiles.push(shoot);
  } else {
    villainMissiles.push(shoot);
  }
};

//función para movimiento de disparos
//En dependencia de si encuentra un choque o no se le permite el movimiento
moveShoot = () => {
  const maxTop = document
    .querySelector(".gameContainer")
    .getBoundingClientRect().top;

  const newMissiles = missiles.filter(missile => {
    if (missile.getBoundingClientRect().top <= maxTop) {
      if (missile.parentNode) {
        missile.parentNode.removeChild(missile);
      }
    }

    return missile.getBoundingClientRect().top > maxTop;
  });

  newMissiles.forEach(missile => {
    const coords = missile.getBoundingClientRect();
    missile.style.top = `${coords.top - 20}px`;
    const impacted = makeImpact(missile, "theVillian");
    if (impacted) impacted.parentElement.removeChild(impacted);
  });
};

//función para movimiento de disparos del villano
//En dependencia de si encuentra un choque o no se le permite el movimiento
moveVillainShoot = () => {
  const maxBottom = document
    .querySelector(".gameContainer")
    .getBoundingClientRect().bottom;

  const newEnemyMissiles = villainMissiles.filter(missile => {
    if (missile.getBoundingClientRect().bottom + 20 >= maxBottom) {
      if (missile.parentNode) {
        missile.parentNode.removeChild(missile);
      }
    }

    return missile.getBoundingClientRect().bottom < maxBottom;
  });

  newEnemyMissiles.forEach(missile => {
    const coords = missile.getBoundingClientRect();
    missile.style.top = `${coords.top + 20}px`;
    const impacted = makeImpact(missile, "heroe_div");
  });
};

//Movimiento del disparo del villano
villainsShoot = () => {
  const villains = document.getElementsByClassName("theVillian");
  let minHeigth = 0;

  for (let index = 0; index < villains.length; index++) {
    const element = villains[index];
    if (element.getBoundingClientRect().top > minHeigth) {
      minHeigth = element.getBoundingClientRect().top;
    }
  }

  for (let index = 0; index < villains.length; index++) {
    const element = villains[index];
    if (element.getBoundingClientRect().top === minHeigth) {
      const a = Math.random();
      if (a > 0.5) {
        genShoot(element, "down");
      }
    }
  }
};
