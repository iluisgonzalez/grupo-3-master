## Integrantes del grupo.

* Alvarado, David
* Cuellar, Jaime
* Cardona, Juan
* Gonzales, Luis

## Desglose de tareas

* Crear el EventEmitter que registra el evento en la función `on`.
 Asignado a Alvarado Rodríguez, David

* Crear el servidor que se pone a la escucha con el `listen`.
 Asignado a Alvarado Rodríguez, David

* Guardar los datos que recibe el socket en un buffer.
 Asignado a Alvarado Rodríguez, David

* Parseado del buffer a string `parseBuffer()`.
Asignado a Gonzalez Siliezar, Luis.

* Implementacion `isfinished()`
Asignado a Gonzalez Siliezar, Luis.

* Descomponer objeto para conocer el tipo de dato, url y header `parsetoObject()`.
Asignado a Cardona Villatoro, Juan Fernando

* Solución a código Test TRAMPA.
Asignado a Cardona Villatoro, Juan Fernando

* Emitir evento con respuesta al cliente 
Asignado a Cuellar Diaz, Jaime

*Hacer el parse a http `parseToHttp`
Asignado a Cuellar  Diaz, Jaime