Ejercicio 10-24

El ejercicio consiste en implementar un servidor HTTP en NodeJS.
Fecha de entrega

El _pull request_ para la entrega del ejercicio debe crearse antes de `2019-11-14T18:25:00+01:00`.
Enunciado

Modifica únicamente el archivo create_server.js para que la función exportada, createServer, pueda ser utilizada tal y como se demuestra en el ejemplo test_server.js.

Para validar el ejercicio, el alumno puede ejecutar en paralelo test_server.js y test_client.js. Al hacerlo, debería aparecer en en el terminal un mensaje de éxito.

Para simplificar el ejercicio, se debe asumir que las siguintes condiciones se aplican:

    El body de un mensaje HTTP termina con dos líneas en blanco.
    Cada conexión se cerrará después de la primera respuesta HTTP.
    No es necesario que el servidor maneje más de una conexión concurrente.

Tecnologías

El ejercicio se debe realizar sin utilizar librerías externas.

El ejercicio se debe realizar utilizando únicamente los módulos de NodeJS explicados hasta ahora en clase:

    events
    Buffer
    Stream
    net

Entregables

Todos los archivos de esta carpeta son entregrables. La carpeta debe incluir, al menos,

    Un archivo create_server.js con la solución del ejercicio.
    Un archivo meta.md con el desglose de tareas por alumno, etc.

Cada alumno debe hacer al menos un commit por cada tarea que tenga asignada.

El método de entrega será, para cada alumno, un pull request a su rama master (/alumno/xxx.yyy/master). El PR puede hacerse desde un fork si es preciso.